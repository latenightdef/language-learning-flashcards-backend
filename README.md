![Synapz Logo](https://gitlab.com/latenightdef/language-learning-flashcards/-/raw/main/src/assets/owl.png)
### Synapz - Language Learning Flashcards

Backend สำหรับ [Synapz](https://gitlab.com/latenightdef/language-learning-flashcards) เขียนด้วย [NestJS](https://nestjs.com/)

โปรเจกต์นี้เป็นส่วนหนึ่งของวิชา THEORY OF COMPUTATION

# การรันโปรเจกต์

1. โคลนโปรเจกต์

```
$ https://gitlab.com/latenightdef/language-learning-flashcards-backend
$ cd language-learning-flashcards-backend
```

2. รันโปรเจกต์

```
$ yarn
$ yarn run start
```