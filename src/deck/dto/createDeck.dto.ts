import { Type } from 'class-transformer';
import {
  IsString,
  IsNotEmpty,
  IsArray,
  // ArrayNotEmpty,
  ValidateNested,
} from 'class-validator';
class Card {
  @IsString()
  @IsNotEmpty()
  word: string;

  @IsString()
  @IsNotEmpty()
  pronunciationLanguage: string;

  @IsString()
  @IsNotEmpty()
  pronunciationEng: string;

  @IsString()
  @IsNotEmpty()
  pronunciationThai: string;

  @IsString()
  @IsNotEmpty()
  translation: string;

  state: number;
}

export class CreateDeckDto {
  @IsString()
  @IsNotEmpty()
  nameInLanguage: string;

  @IsString()
  @IsNotEmpty()
  nameEng: string;

  @IsString()
  @IsNotEmpty()
  nameThai: string;

  @IsArray()
  //@ArrayNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => Card)
  cards: Card[];
}
