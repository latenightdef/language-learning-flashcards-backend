import {
  Controller,
  Body,
  Get,
  Param,
  Post,
  UseGuards,
  Delete,
  Patch,
} from '@nestjs/common';
import { DeckService } from './deck.service';
import { GetUser } from '../auth/decorator';
import { JwtGuard } from 'src/auth/guard';
import { CreateDeckDto } from './dto/createDeck.dto';
import { CreateCardDto, UpdateCardDto, UpdateDeckDto } from './dto';

@Controller('deck')
export class DeckController {
  constructor(private deckService: DeckService) {}

  @UseGuards(JwtGuard)
  @Get()
  findAllUserDeck(@GetUser('id') userId: number) {
    return this.deckService.findAllUserDeck(userId);
  }

  @Get('starter')
  findAllStarterDeck() {
    return this.deckService.findAllStarterDeck();
  }

  @UseGuards(JwtGuard)
  @Get('starter/expData')
  findExpData(@GetUser('id') userId: number) {
    return this.deckService.findExpData(userId);
  }

  @UseGuards(JwtGuard)
  @Get(':id')
  findCard(@GetUser('id') userId: number, @Param('id') deckidStr: string) {
    return this.deckService.findCard(userId, deckidStr);
  }

  @Get('starter/:id')
  findStarterCard(@Param('id') deckidStr: string) {
    return this.deckService.findStarterCard(deckidStr);
  }

  @UseGuards(JwtGuard)
  @Post('create')
  createDeck(@GetUser('id') userId: number, @Body() dto: CreateDeckDto) {
    return this.deckService.createDeck(userId, dto);
  }

  @UseGuards(JwtGuard)
  @Patch('update/:id')
  updateDeck(
    @GetUser('id') userId: number,
    @Body() dto: UpdateDeckDto,
    @Param('id') deckidStr: string,
  ) {
    return this.deckService.updateDeck(userId, dto, deckidStr);
  }

  @UseGuards(JwtGuard)
  @Delete('delete/:id')
  deleteDeck(@GetUser('id') userId: number, @Param('id') deckidStr: string) {
    return this.deckService.deleteDeck(userId, deckidStr);
  }

  @UseGuards(JwtGuard)
  @Post('card/create/:id')
  createCard(
    @GetUser('id') userId: number,
    @Body() dto: CreateCardDto,
    @Param('id') deckidStr: string,
  ) {
    return this.deckService.createCard(userId, dto, deckidStr);
  }

  @UseGuards(JwtGuard)
  @Delete('card/delete/:id')
  deleteCard(@GetUser('id') userId: number, @Param('id') cardidStr: string) {
    return this.deckService.deleteCard(userId, cardidStr);
  }

  @UseGuards(JwtGuard)
  @Patch('card/update/:id')
  updateCard(
    @GetUser('id') userId: number,
    @Body() dto: UpdateCardDto,
    @Param('id') cardidStr: string,
  ) {
    return this.deckService.updateCard(userId, dto, cardidStr);
  }
}
