import { Injectable, ForbiddenException } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateDeckDto } from './dto/createDeck.dto';
import { CreateCardDto, UpdateCardDto, UpdateDeckDto } from './dto';
@Injectable()
export class DeckService {
  constructor(private prisma: PrismaService) {}

  async findAllUserDeck(userId: number) {
    const decks = await this.prisma.deck.findMany({
      where: { userId: userId },
    });
    return decks;
  }

  async findAllStarterDeck() {
    const decks = await this.prisma.starterDeck.findMany({});
    return decks;
  }

  async findExpData(userId: number) {
    const data: any[] = [];
    const decks = await this.prisma.starterDeck.findMany({});
    for (const deck of decks) {
      //iterate throught starter deck if don't has relation then create it
      let deckRelation = await this.prisma.userAndStarterDeckRelation.findFirst(
        {
          where: { deckId: deck.id },
        },
      );
      if (!deckRelation)
        deckRelation = await this.prisma.userAndStarterDeckRelation.create({
          data: {
            userId: userId,
            deckId: deck.id,
            experince: 0,
            maxExperince: 10000,
            playCount: 0,
            totalEasy: 0,
            totalHard: 0,
            totalSkip: 0,
            easy: 0,
            hard: 0,
            skip: 0,
            nowIdx: 0,
            maxIdx: 0,
          },
        });
      delete deckRelation.id;
      delete deckRelation.userId;
      delete deckRelation.deckId;
      const onedata = {
        ...deck,
        ...deckRelation,
      };
      data.push(onedata);
    }
    return data;
  }

  async findCard(userId: number, deckidStr: string) {
    const deckid = Number(deckidStr);
    const check = await this.prisma.deck.findUnique({
      where: { id: deckid, userId: userId },
    });
    if (!check) throw new ForbiddenException('No decks to find');
    const cards = await this.prisma.card.findMany({
      where: { deckId: deckid },
    });
    return cards;
  }

  async findStarterCard(deckidStr: string) {
    const deckid = Number(deckidStr);
    const check = await this.prisma.starterDeck.findUnique({
      where: { id: deckid },
    });
    if (!check) throw new ForbiddenException('No decks to find');
    const cards = await this.prisma.starterCard.findMany({
      where: { deckId: deckid },
    });
    return cards;
  }

  async createDeck(userId: number, dto: CreateDeckDto) {
    const data = dto.cards.map((Card) => ({
      word: Card.word,
      pronunciationLanguage: Card.pronunciationLanguage,
      pronunciationEng: Card.pronunciationEng,
      pronunciationThai: Card.pronunciationThai,
      translation: Card.translation,
      state: 0,
      selected: 0,
    }));

    const deck = await this.prisma.deck.create({
      data: {
        nameInLanguage: dto.nameInLanguage,
        nameEng: dto.nameEng,
        nameThai: dto.nameThai,
        cards: {
          create: data,
        },
        userId: userId,
        experince: 0,
        maxExperince: 10000,
        playCount: 0,
        totalEasy: 0,
        totalHard: 0,
        totalSkip: 0,
        easy: 0,
        hard: 0,
        skip: 0,
        nowIdx: 0,
        maxIdx: 0,
      },
    });
    return deck;
  }

  async updateDeck(userId: number, dto: UpdateDeckDto, deckidStr: string) {
    const deckid = Number(deckidStr);
    const check = await this.prisma.deck.findUnique({
      where: { id: deckid, userId: userId },
    });
    if (!check) throw new ForbiddenException('No decks to update');
    const updateDeck = await this.prisma.deck.update({
      where: { id: deckid, userId: userId },
      data: {
        nameInLanguage: dto.nameInLanguage,
        nameEng: dto.nameEng,
        nameThai: dto.nameThai,
      },
    });
    dto.cards.forEach(async (card) => {
      if (card.id) {
        const check = await this.prisma.card.findUnique({
          where: { id: card.id, deckId: deckid },
        });
        if (!check) throw new ForbiddenException('No Card with that id!!!');
        // if no word pronunciation and translation delete card by id
        if (
          !card.word &&
          !card.pronunciationEng &&
          !card.pronunciationLanguage &&
          !card.pronunciationThai &&
          !card.translation
        ) {
          await this.prisma.card.delete({
            where: { id: card.id },
          });
        } else {
          // if have word pronunciation or translation update card by id
          await this.prisma.card.update({
            where: { id: card.id },
            data: {
              word: card.word,
              pronunciationLanguage: card.pronunciationLanguage,
              pronunciationEng: card.pronunciationEng,
              pronunciationThai: card.pronunciationThai,
              translation: card.translation,
            },
          });
        }
      } else {
        // if not provide id -> create new card
        if (!card.word)
          throw new ForbiddenException('Cannot create card with no word');
        if (
          !card.pronunciationLanguage ||
          !card.pronunciationThai ||
          !card.pronunciationEng
        )
          throw new ForbiddenException(
            'Cannot create card with no pronunciation',
          );
        if (!card.translation)
          throw new ForbiddenException(
            'Cannot create card with no translation',
          );
        await this.prisma.card.create({
          data: {
            word: card.word,
            pronunciationLanguage: card.pronunciationLanguage,
            pronunciationEng: card.pronunciationEng,
            pronunciationThai: card.pronunciationThai,
            translation: card.translation,
            state: 0,
            deckId: deckid,
            selected: 0,
          },
        });
      }
    });
    return updateDeck;
  }

  async deleteDeck(userId: number, deckidStr: string) {
    const deckid = Number(deckidStr);
    const check = await this.prisma.deck.findUnique({
      where: { id: deckid, userId: userId },
    });
    if (!check) throw new ForbiddenException('No decks to delete');
    await this.prisma.deck.update({
      where: {
        id: deckid,
        userId: userId,
      },
      data: {
        cards: {
          deleteMany: {},
        },
      },
    });
    const deleted = await this.prisma.deck.delete({
      where: { id: deckid },
    });
    return deleted;
  }

  async createCard(userId: number, dto: CreateCardDto, deckidStr: string) {
    const deckid = Number(deckidStr);
    const check = await this.prisma.deck.findUnique({
      where: { id: deckid, userId: userId },
    });
    if (!check) throw new ForbiddenException('No decks to create card');
    const data = {
      word: dto.word,
      pronunciationLanguage: dto.pronunciationLanguage,
      pronunciationEng: dto.pronunciationEng,
      pronunciationThai: dto.pronunciationThai,
      translation: dto.translation,
      state: 0,
      deckId: deckid,
      selected: 0,
    };
    const createCard = this.prisma.card.create({ data: data });
    return createCard;
  }

  async deleteCard(userId: number, cardidStr: string) {
    const cardid = Number(cardidStr);
    const deletecard = await this.prisma.card.delete({
      where: { id: cardid },
    });
    if (!deletecard) throw new ForbiddenException('No card to delete');
    return deletecard;
  }

  async updateCard(userId: number, dto: UpdateCardDto, cardidStr: string) {
    const cardid = Number(cardidStr);
    const check = await this.prisma.card.findUnique({ where: { id: cardid } });
    if (!check) throw new ForbiddenException('No card to update');
    const updateCard = await this.prisma.card.update({
      where: { id: cardid },
      data: {
        ...dto,
      },
    });
    return updateCard;
  }
}
