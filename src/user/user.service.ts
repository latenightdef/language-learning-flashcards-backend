import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { EditUserDto } from './dto';
import { User } from '@prisma/client';
import * as argon from 'argon2';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  async getUser(user: User) {
    const top = await this.prisma.user.findMany({
      orderBy: {
        totalExp: 'desc',
      },
    });
    let i: number = 1;
    for (const x of top) {
      delete x.password;
      if (x.id == user.id) {
        return {
          ranking: i,
          ...x,
        };
      }
      i++;
    }
  }

  async editUser(userId: number, dto: EditUserDto) {
    let update = await this.prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        username: dto.username,
        nickname: dto.nickname,
        profileImage: dto.profileImage,
      },
    });
    if (dto.password && dto.newPassword) {
      const user = await this.prisma.user.findUnique({
        where: {
          id: userId,
        },
      });
      const pwMatches = await argon.verify(user.password, dto.password);
      if (!pwMatches) throw new ForbiddenException('old password wrong');
      const newPassword = await argon.hash(dto.newPassword);
      update = await this.prisma.user.update({
        where: {
          id: userId,
        },
        data: {
          password: newPassword,
        },
      });
    }
    delete update.password;
    return update;
  }

  async leaderboard(size: string) {
    const s = Number(size);
    const top = await this.prisma.user.findMany({
      take: s,
      orderBy: {
        totalExp: 'desc',
      },
    });
    const data: any[] = [];
    let i: number = 1;
    top.forEach((x) => {
      delete x.password;
      data.push({
        ranking: i,
        ...x,
      });
      i++;
    });
    return data;
  }
}
