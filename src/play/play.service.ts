import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { sendDeckDto } from './dto/sendDeckDto';

@Injectable()
export class PlayService {
  constructor(private prisma: PrismaService) {}

  async getCard(userId: number, deckidStr: string, first: string) {
    const deckid = Number(deckidStr);
    const deck = await this.prisma.deck.findUnique({
      where: { id: deckid, userId: userId },
    });
    if (!deck) throw new ForbiddenException('No deck with that id');
    const cards = await this.prisma.card.findMany({
      where: { deckId: deckid, selected: 0 },
    });
    // if no card return
    if (cards.length == 0)
      throw new ForbiddenException('this deck has no card!!!');
    // if first set idx = 0
    if (first && deck.nowIdx != 0) {
      deck.nowIdx = 0;
    }
    //first card to get
    if (deck.nowIdx == 0) {
      deck.maxIdx = cards.length < 10 ? cards.length : 10;
      await this.prisma.deck.update({
        where: { id: deck.id },
        data: { easy: 0, hard: 0, skip: 0 },
      });
    }
    //get card
    deck.nowIdx++;
    let sumState = 0;
    cards.forEach((card) => {
      sumState += card.state + 1;
    });
    sumState = Math.floor(Math.random() * sumState);
    for (const card of cards) {
      sumState -= card.state + 1;
      if (sumState < 0) {
        await this.prisma.deck.update({
          where: { id: deckid, userId: userId },
          data: {
            nowIdx: deck.nowIdx,
            maxIdx: deck.maxIdx,
          },
        });
        await this.prisma.card.update({
          where: { id: card.id },
          data: { selected: 1 },
        });
        const data = {
          card,
          nowIdx: deck.nowIdx,
          maxIdx: deck.maxIdx,
        };
        return data;
      }
    }
  }

  async sendCard(userId: number, cardidStr: string, level: string) {
    const cardid = Number(cardidStr);
    const card = await this.prisma.card.findUnique({ where: { id: cardid } });
    if (!card) throw new ForbiddenException('no card with that id');
    const deck = await this.prisma.deck.findUnique({
      where: { id: card.deckId },
    });
    if (level == 'easy') {
      //ez
      card.state++;
      deck.easy++;
    } else if (level == 'hard') {
      //hard
      deck.hard++;
    } else if (level == 'skip') {
      //skip
      card.state = card.state - 1 < 0 ? 0 : card.state - 1;
      deck.skip++;
    } else {
      throw new ForbiddenException(
        'there is no such a difficulity check your typo',
      );
    }
    //update
    await this.prisma.card.update({
      where: { id: cardid },
      data: { state: card.state },
    });
    await this.prisma.deck.update({
      where: { id: deck.id },
      data: { easy: deck.easy, hard: deck.hard, skip: deck.skip },
    });
    //================
    //if this is the last card
    if (deck.nowIdx == deck.maxIdx) {
      //reset all selected card and update deck conclusion data
      await this.prisma.card.updateMany({
        where: { deckId: deck.id },
        data: { selected: 0 },
      });
      await this.prisma.deck.update({
        where: { id: deck.id },
        data: {
          easy: 0,
          hard: 0,
          skip: 0,
          totalEasy: deck.totalEasy + deck.easy,
          totalHard: deck.totalHard + deck.hard,
          totalSkip: deck.totalSkip + deck.skip,
          playCount: deck.playCount + 1,
          experince: deck.experince + 100,
        },
      });
      //add user total exp
      await this.prisma.user.update({
        where: { id: userId },
        data: { totalExp: { increment: 100 } },
      });
      //data
      const data = {
        easy: deck.easy,
        hard: deck.hard,
        skip: deck.skip,
      };
      return data;
    }
    return 'ok';
  }

  async startergetCard(userId: number, deckidStr: string, first: string) {
    const deckid = Number(deckidStr);
    const deck = await this.prisma.starterDeck.findUnique({
      where: { id: deckid },
    });
    if (!deck) throw new ForbiddenException('No starter deck with that id');
    const cards = await this.prisma.starterCard.findMany({
      where: { deckId: deckid },
    });
    if (cards.length == 0)
      throw new ForbiddenException('this deck has no card!!!');
    //find deck relation
    let deckRelation = await this.prisma.userAndStarterDeckRelation.findFirst({
      where: { userId: userId, deckId: deckid },
    });
    //if relation not exist then create it
    if (!deckRelation)
      deckRelation = await this.prisma.userAndStarterDeckRelation.create({
        data: {
          userId: userId,
          deckId: deckid,
          experince: 0,
          maxExperince: 10000,
          playCount: 0,
          totalEasy: 0,
          totalHard: 0,
          totalSkip: 0,
          easy: 0,
          hard: 0,
          skip: 0,
          nowIdx: 0,
          maxIdx: 0,
        },
      });
    //find card relation
    let cardsRelation = await this.prisma.userAndStarterCardRelation.findMany({
      where: { userId: userId, fromDeckId: deckid },
    });
    //if relation not exist then create it
    if (!cardsRelation || cardsRelation.length == 0) {
      for (const card of cards) {
        await this.prisma.userAndStarterCardRelation.create({
          data: {
            userId: userId,
            fromDeckId: deckid,
            cardId: card.id,
            state: 0,
            selected: 0,
          },
        });
      }
    }
    //get all card relation that not selected
    cardsRelation = await this.prisma.userAndStarterCardRelation.findMany({
      where: { userId: userId, fromDeckId: deckid, selected: 0 },
    });

    //if first set idx=0
    if (first && deckRelation.nowIdx != 0) {
      deckRelation.nowIdx = 0;
    }
    //first card to get
    if (deckRelation.nowIdx == 0) {
      deckRelation.maxIdx = cards.length < 10 ? cards.length : 10;
      await this.prisma.userAndStarterDeckRelation.update({
        where: { id: deckRelation.id },
        data: { easy: 0, hard: 0, skip: 0 },
      });
    }
    //get card
    deckRelation.nowIdx++;
    let sumState = 0;
    cardsRelation.forEach((card) => {
      sumState += card.state + 1;
    });
    sumState = Math.floor(Math.random() * sumState);
    for (const card of cardsRelation) {
      sumState -= card.state + 1;
      if (sumState < 0) {
        await this.prisma.userAndStarterDeckRelation.update({
          where: { id: deckRelation.id, userId: userId },
          data: {
            nowIdx: deckRelation.nowIdx,
            maxIdx: deckRelation.maxIdx,
          },
        });
        await this.prisma.userAndStarterCardRelation.update({
          where: { id: card.id },
          data: { selected: 1 },
        });
        const select_card = await this.prisma.starterCard.findUnique({
          where: { id: card.cardId },
        });
        const data = {
          select_card,
          nowIdx: deckRelation.nowIdx,
          maxIdx: deckRelation.maxIdx,
        };
        return data;
      }
    }
  }

  async startersendCard(userId: number, cardidStr: string, level: string) {
    const cardid = Number(cardidStr);
    const card = await this.prisma.starterCard.findUnique({
      where: { id: cardid },
    });
    if (!card) throw new ForbiddenException('no card with that id');
    const cardRelation = await this.prisma.userAndStarterCardRelation.findFirst(
      { where: { cardId: cardid } },
    );
    const deckRelation = await this.prisma.userAndStarterDeckRelation.findFirst(
      { where: { deckId: cardRelation.fromDeckId } },
    );
    if (level == 'easy') {
      //ez
      cardRelation.state++;
      deckRelation.easy++;
    } else if (level == 'hard') {
      //hard
      deckRelation.hard++;
    } else if (level == 'skip') {
      //skip
      cardRelation.state =
        cardRelation.state - 1 < 0 ? 0 : cardRelation.state - 1;
      deckRelation.skip++;
    } else {
      throw new ForbiddenException(
        'there is no such a difficulity check your typo',
      );
    } //update
    await this.prisma.userAndStarterCardRelation.update({
      where: { id: cardRelation.id },
      data: {
        state: cardRelation.state,
      },
    });
    await this.prisma.userAndStarterDeckRelation.update({
      where: { id: deckRelation.id },
      data: {
        easy: deckRelation.easy,
        hard: deckRelation.hard,
        skip: deckRelation.skip,
      },
    });
    //if this is the last card
    if (deckRelation.nowIdx == deckRelation.maxIdx) {
      //reset all selected card and update deck conclusion data
      await this.prisma.userAndStarterCardRelation.updateMany({
        where: { fromDeckId: deckRelation.deckId },
        data: { selected: 0 },
      });
      await this.prisma.userAndStarterDeckRelation.update({
        where: { id: deckRelation.id },
        data: {
          easy: 0,
          hard: 0,
          skip: 0,
          totalEasy: deckRelation.totalEasy + deckRelation.easy,
          totalHard: deckRelation.totalHard + deckRelation.hard,
          totalSkip: deckRelation.totalSkip + deckRelation.skip,
          playCount: deckRelation.playCount + 1,
          experince: deckRelation.experince + 100,
        },
      });
      //add user total exp
      await this.prisma.user.update({
        where: { id: userId },
        data: { totalExp: { increment: 100 } },
      });
      //data
      const data = {
        easy: deckRelation.easy,
        hard: deckRelation.hard,
        skip: deckRelation.skip,
      };
      return data;
    }
    return 'ok';
  }

  async getDeck(userId: number, deckidStr: string) {
    const data: any[] = [];
    const deckid = Number(deckidStr);
    const deck = await this.prisma.deck.findUnique({
      where: { id: deckid, userId: userId },
    });
    if (!deck) throw new ForbiddenException('No deck with that id');
    const cards = await this.prisma.card.findMany({
      where: { deckId: deckid },
    });
    // if no card return
    if (cards.length == 0)
      throw new ForbiddenException('this deck has no card!!!');
    const length = cards.length < 10 ? cards.length : 10;
    let sumState = 0;
    cards.forEach((card) => {
      sumState += card.state + 1;
    });
    for (let i = 0; i < length; i++) {
      let now = Math.floor(Math.random() * sumState);
      console.log(now);
      for (let j = 0; j < cards.length; j++) {
        now -= cards[j].state + 1;
        if (now < 0) {
          data.push(cards[j]);
          sumState -= cards[j].state + 1;
          cards.splice(j, 1);
          break;
        }
      }
    }
    return data;
  }
  async sendDeck(userId: number, deckidStr: string, dto: sendDeckDto) {
    const deckid = Number(deckidStr);
    const deck = await this.prisma.deck.findUnique({
      where: { id: deckid, userId: userId },
    });
    if (!deck) throw new ForbiddenException('No deck with that id');
    let easy = 0,
      hard = 0,
      skip = 0;
    for (const card of dto.cards) {
      const cardData = await this.prisma.card.findUnique({
        where: { id: card.id, deckId: deckid },
      });
      if (!card) throw new ForbiddenException('no card with id:' + card.id);
      if (card.state == 'skip') {
        await this.prisma.card.update({
          where: { id: card.id },
          data: {
            state: cardData.state + 2,
          },
        });
        skip++;
      } else if (card.state == 'hard') {
        await this.prisma.card.update({
          where: { id: card.id },
          data: {
            state: cardData.state + 1,
          },
        });
        hard++;
      } else if (card.state == 'easy') {
        const tmp = cardData.state - 1 < 0 ? 0 : cardData.state - 1;
        await this.prisma.card.update({
          where: { id: card.id },
          data: {
            state: tmp,
          },
        });
        easy++;
      } else {
        throw new ForbiddenException(
          'there is no such a difficulity check your typo',
        );
      }
    }
    await this.prisma.deck.update({
      where: { id: deck.id },
      data: {
        easy: 0,
        hard: 0,
        skip: 0,
        totalEasy: deck.totalEasy + easy,
        totalHard: deck.totalHard + hard,
        totalSkip: deck.totalSkip + skip,
        playCount: deck.playCount + 1,
        experince: deck.experince + 100,
      },
    });
    await this.prisma.user.update({
      where: { id: userId },
      data: { totalExp: { increment: 100 } },
    });
    return 'ok';
  }
  async getstarterDeck(userId: number, deckidStr: string) {
    const data: any[] = [];
    const deckid = Number(deckidStr);
    const deck = await this.prisma.starterDeck.findUnique({
      where: { id: deckid },
    });
    if (!deck) throw new ForbiddenException('No starter deck with that id');
    const cards = await this.prisma.starterCard.findMany({
      where: { deckId: deckid },
    });
    if (cards.length == 0)
      throw new ForbiddenException('this deck has no card!!!');
    //find deck relation
    let deckRelation = await this.prisma.userAndStarterDeckRelation.findFirst({
      where: { userId: userId, deckId: deckid },
    });
    //if relation not exist then create it
    if (!deckRelation)
      deckRelation = await this.prisma.userAndStarterDeckRelation.create({
        data: {
          userId: userId,
          deckId: deckid,
          experince: 0,
          maxExperince: 10000,
          playCount: 0,
          totalEasy: 0,
          totalHard: 0,
          totalSkip: 0,
          easy: 0,
          hard: 0,
          skip: 0,
          nowIdx: 0,
          maxIdx: 0,
        },
      });
    //find card relation
    let cardsRelation = await this.prisma.userAndStarterCardRelation.findMany({
      where: { userId: userId, fromDeckId: deckid },
    });
    //if relation not exist then create it
    if (!cardsRelation || cardsRelation.length == 0) {
      for (const card of cards) {
        await this.prisma.userAndStarterCardRelation.create({
          data: {
            userId: userId,
            fromDeckId: deckid,
            cardId: card.id,
            state: 0,
            selected: 0,
          },
        });
      }
    }
    //get all card relation
    cardsRelation = await this.prisma.userAndStarterCardRelation.findMany({
      where: { userId: userId, fromDeckId: deckid },
    });
    const length = cards.length < 10 ? cards.length : 10;
    let sumState = 0;
    cardsRelation.forEach((card) => {
      sumState += card.state + 1;
    });
    for (let i = 0; i < length; i++) {
      let now = Math.floor(Math.random() * sumState);
      // console.log(now);
      for (let j = 0; j < cardsRelation.length; j++) {
        now -= cardsRelation[j].state + 1;
        if (now < 0) {
          data.push(
            await this.prisma.starterCard.findUnique({
              where: { id: cardsRelation[j].cardId },
            }),
          );
          sumState -= cardsRelation[j].state + 1;
          cardsRelation.splice(j, 1);
          break;
        }
      }
    }
    return data;
  }
  async sendstarterDeck(userId: number, deckidStr: string, dto: sendDeckDto) {
    const deckid = Number(deckidStr);
    const deck = await this.prisma.starterDeck.findUnique({
      where: { id: deckid },
    });
    if (!deck) throw new ForbiddenException('No deck with that id');
    const deckRelation = await this.prisma.userAndStarterDeckRelation.findFirst(
      {
        where: { userId: userId, deckId: deckid },
      },
    );
    let easy = 0,
      hard = 0,
      skip = 0;
    for (const card of dto.cards) {
      const cardData = await this.prisma.userAndStarterCardRelation.findFirst({
        where: { fromDeckId: deckid, cardId: card.id },
      });
      if (!card) throw new ForbiddenException('no card with id:' + card.id);
      if (card.state == 'skip') {
        await this.prisma.userAndStarterCardRelation.update({
          where: { id: card.id },
          data: {
            state: cardData.state + 2,
          },
        });
        skip++;
      } else if (card.state == 'hard') {
        await this.prisma.userAndStarterCardRelation.update({
          where: { id: card.id },
          data: {
            state: cardData.state + 1,
          },
        });
        hard++;
      } else if (card.state == 'easy') {
        const tmp = cardData.state - 1 < 0 ? 0 : cardData.state - 1;
        await this.prisma.userAndStarterCardRelation.update({
          where: { id: card.id },
          data: {
            state: tmp,
          },
        });
        easy++;
      } else {
        throw new ForbiddenException(
          'there is no such a difficulity check your typo',
        );
      }
    }
    await this.prisma.userAndStarterDeckRelation.update({
      where: { id: deck.id },
      data: {
        easy: 0,
        hard: 0,
        skip: 0,
        totalEasy: deckRelation.totalEasy + easy,
        totalHard: deckRelation.totalHard + hard,
        totalSkip: deckRelation.totalSkip + skip,
        playCount: deckRelation.playCount + 1,
        experince: deckRelation.experince + 100,
      },
    });
    await this.prisma.user.update({
      where: { id: userId },
      data: { totalExp: { increment: 100 } },
    });
    return 'ok';
  }
}
