import { Type } from 'class-transformer';
import {
  IsString,
  IsNotEmpty,
  IsArray,
  IsNumber,
  // ArrayNotEmpty,
  ValidateNested,
} from 'class-validator';
class Card {
  @IsNumber()
  @IsNotEmpty()
  id: number;

  @IsString()
  @IsNotEmpty()
  state: string;
}

export class sendDeckDto {
  @IsArray()
  //@ArrayNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => Card)
  cards: Card[];
}
